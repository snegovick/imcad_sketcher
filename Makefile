PYTHON := PYTHONPATH=$(PYTHONPATH):$(CURDIR) python3

all: help

EXAMPLES_DIR := examples
EXAMPLES := $(patsubst $(EXAMPLES_DIR)/%.py,%,$(wildcard $(EXAMPLES_DIR)/*.py))

define EXAMPLE_RULES
example.$(1): $(EXAMPLES_DIR)/$(1).py
	@$$(PYTHON) $$<
endef

$(foreach EXAMPLE,$(EXAMPLES),$(eval $(call EXAMPLE_RULES,$(EXAMPLE))))

help:
	@echo '$ make example.NAME # Run example NAME, where NAME one of: $(EXAMPLES)'
	@echo
