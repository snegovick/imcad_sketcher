# Immediate-mode CAD

**IMCAD** is an immediate-mode parametric programmatic solid computer aided design system based on *Open CASCADE Technologies*.

Unlike to other similar solutions **IMCAD** is developed with emphasis on widely using sketches.

## Key features

* Immediate operation in *OpenSCAD* manner

    The language built as Python module and proposes immediate operating model.
    This means that when some function called then some operation done.
    Such API style implies using contexts.

    It uses Python's **with** blocks for wrapping code in contexts.
    There are several kinds of contexts:

    - Affine transformation context
    - Sketching context
    - Profile plotting context
    - Solidification contexts (extrusion/revolution/sweeping)
    - Boolean operations contexts
    - ...

    Key advantages of that design is:

    - Declarative style of coding which looks pretty and easy readable
    - Code may be mixed with control blocks like conditions or loops
    - Code may be modularized using function blocks or class methods

* Simple to use and powerfull sketching API

    For sketching you can either use built-in parametric profiles or create custom with useful elements:

    - Straight line segments
    - Fillets and chamfers in corners
    - [TODO] Arcs and parametric curves

* [TODO]

## Comparison with existing solutions

### Why not simply use OpenSCAD

**OpenSCAD** is great programmatic CAD which gets it possible to built
many awesome things in a parametric-driven manner.
But it also has some key disadvantages, like that:

* It operates with triangle meshes not with a generic solid forms

    Triangle meshes is a reasonable compromise for 3D graphics.
    Solid computer aided design requires curves and surfaces.
    You restricted by operations with meshes only.
    You cannot produce *STEP* output only *STL*.

* It doesn't provide useable way to work with 2D sketches

    You only able create polygons using points and edges.

* It haven't advanced solid CAD features like fillet, chamfer, sweep and etc.

### Why not simply use CadQuery

**CadQuery** is an interesting programmatic CAD which also based on *Open CASCADE*
and introduces usable techniques of parametric solid design.

It solves some problems of OpenSCAD by using *Open CASCADE* as geometry kernel.
But it still has disadvantages:

* Working with sketches is not so usable

    The CadQuery has call-chain style API.
    That style is not so readable as *OpenSCAD*-like language.

* Query language still restricted to simple queries

### Editors integration

In order to simplify interacting with advanced code editors and IDE's
the **IMCAD** can be run in server mode.
In this case it can helps generate previews when source code changed.

![IMCAD Preview minor mode for Emacs](assets/imcad-preview-mode.gif)

...to be continued...
