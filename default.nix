{ pkgs ? import <nixpkgs> {} }:
with pkgs;
let
  pythonPackages = python3Packages;
  python = pythonPackages.python;
  pythonocc = callPackage ./nix/pythonocc {
    inherit pythonPackages;
  };
in rec {
  imcad = stdenv.mkDerivation {
    name = "imcad";
    src = ".";
    nativeBuildInputs = [
      stdenv
      python
    ];
    propagatedBuildInputs = with pythonPackages; [
      pythonocc
    ];
  };
}
