;; imcad.el -- Emacs interaction with IMCAD server

(define-minor-mode imcad-preview-mode
  "IMCAD Preview"
  :lighter " IMCAD"
  (if imcad-preview-mode
      (imcad-preview-mode-init)
    (imcad-preview-mode-fini)))

(defvar-local imcad-preview-editor-buffer nil
  "The editor buffer linked with viewer one.")
(defvar-local imcad-preview-viewer-buffer nil
  "The viewer buffer linked with editor one.")
(defvar-local imcad-preview-viewer-params nil
  "The viewer parameters.")

(defvar imcad-preview-process-handle nil
  "IMCAD server process handle")

(defun imcad-preview-mode-init ()
  ;; link the editor with viewer using local variable
  (setq imcad-preview-viewer-buffer
        (get-buffer-create (concat (buffer-name) ":preview")))
  ;; prepare viewer buffer
  (let ((editor-buffer (current-buffer)))
    (with-current-buffer imcad-preview-viewer-buffer
      ;; activate major mode
      (imcad-preview-display-mode)
      ;; link the viewer with editor using local variable
      (setq imcad-preview-editor-buffer editor-buffer)
      ;; initialize view parameters
      (imcad-preview-viewer-params-init)))
  ;; configure preview window
  (if imcad-preview-window-placement
      (let* ((editor-window (get-buffer-window))
             (viewer-size (round (* imcad-preview-window-proportion
                                    (window-size editor-window
                                                 (or (eq 'left imcad-preview-window-placement)
                                                     (eq 'right imcad-preview-window-placement))
                                                 't))))
             (viewer-window (split-window editor-window (- viewer-size)
                                          imcad-preview-window-placement 't)))
        ;; preconfigure window
        (set-window-margins viewer-window 0 0)
        (set-window-scroll-bars viewer-window nil nil nil nil)
        ;; attach preview buffer to window
        (set-window-buffer viewer-window imcad-preview-viewer-buffer 't)
        ;; makes window dedicated to permit buffer switching
        (set-window-dedicated-p viewer-window 't)
        ;; configure window
        (set-window-margins viewer-window 0 0)
        (set-window-scroll-bars viewer-window nil nil nil nil)
        ;; disable cursor blinking
        (blink-cursor-mode 0)
        ;; hide cursor for the viewer window
        (internal-show-cursor viewer-window nil)))
  ;; trigger initial refresh
  (imcad-preview-refresh))

(defun imcad-preview-mode-fini ()
  ;; unregister file save hook
  (remove-hook 'after-save-hook 'imcad-preview-refresh 't)
  ;; cleanup preview
  (delete-window (get-buffer-window imcad-preview-viewer-buffer))
  (kill-buffer imcad-preview-viewer-buffer)
  (kill-local-variable 'imcad-preview-viewer-buffer)
  (imcad-preview-process-stop-when-unused))

(defun imcad-preview-viewer-params-init ()
  (setq imcad-preview-viewer-params
        (list :proj imcad-preview-default-proj
              :mode imcad-preview-default-mode
              :hrot (car imcad-preview-default-rot)
              :vrot (cdr imcad-preview-default-rot)
              :hpan (car imcad-preview-default-pan)
              :vpan (cdr imcad-preview-default-pan)
              :fit imcad-preview-default-fit
              :zoom imcad-preview-default-zoom)))

(defun imcad-preview-display-param-get (name)
  (plist-get imcad-preview-viewer-params name))

(defun imcad-preview-display-param-set (name value &rest other)
  (cond ((eq :view name) (imcad-preview-display-param-set-helper
                          :hrot (car value) (cons :vrot (cons (cdr value) other))))
        ('t (imcad-preview-display-param-set-helper name value other)))
  (imcad-preview-display-refresh))

(defun imcad-preview-display-param-set-helper (name value other)
  (plist-put imcad-preview-viewer-params name
             (imcad-preview-display-param-fix name value))
  (if other
      (imcad-preview-display-param-set-helper (car other) (cadr other) (cddr other))))

(defun imcad-preview-display-param-fix (name value)
  (pcase name
    ;; clamp longitude (horizontal rotation) to range -180..+180
    (:hrot (if (> -180 value) (imcad-preview-display-param-fix name (+ value 360))
             (if (< 180 value) (imcad-preview-display-param-fix name (- value 360))
               value)))
    ;; clamp latitude (vertical rotation) to range -90..+90
    (:vrot (if (> -90 value) -90
             (if (< 90 value) 90
               value)))
    (other value)))

(defun imcad-preview-display-param-adj (name delta)
  (imcad-preview-display-param-set
   name (+ delta (imcad-preview-display-param-get name))))

(defun imcad-preview-display-reset-view ()
  (interactive "@")
  (imcad-preview-display-param-set :hrot (car imcad-preview-default-rot)
                                   :vrot (cdr imcad-preview-default-rot)
                                   :hpan (car imcad-preview-default-pan)
                                   :vpan (cdr imcad-preview-default-pan)))

(defmacro imcad-preview-display-mod-commands (op name &rest values)
  `(progn
     ,@(mapcar
        (lambda (value)
          `(defun ,(intern (concat "imcad-preview-display-"
                                   (symbol-name op) "-"
                                   (substring (symbol-name name) 1) "-"
                                   (symbol-name (if (consp value)
                                                    (car value)
                                                  value)))) ()
             (interactive "@")
             (,(intern (concat "imcad-preview-display-param-" (symbol-name op)))
              ,name ,(if (consp value) (cdr value) `',value))
             ))
        values)))

(imcad-preview-display-mod-commands set :mode shaded wireframe hlr)
(imcad-preview-display-mod-commands set :proj ortho persp)
(imcad-preview-display-mod-commands set :view
                                    (iso . imcad-preview-rot-view-iso)
                                    (front . imcad-preview-rot-view-front)
                                    (left . imcad-preview-rot-view-left)
                                    (top . imcad-preview-rot-view-top)
                                    (rear . imcad-preview-rot-view-rear)
                                    (right . imcad-preview-rot-view-right)
                                    (bottom . imcad-preview-rot-view-bottom))

(imcad-preview-display-mod-commands adj :hrot
                                    (left . (car imcad-preview-rot-step))
                                    (right . (- (car imcad-preview-rot-step))))
(imcad-preview-display-mod-commands adj :vrot
                                    (up . (- (cdr imcad-preview-rot-step)))
                                    (down . (cdr imcad-preview-rot-step)))
(imcad-preview-display-mod-commands adj :hpan
                                    (left . (* (car (imcad-preview-display-image-size)) (car imcad-preview-pan-step)))
                                    (right . (- (* (car (imcad-preview-display-image-size)) (car imcad-preview-pan-step)))))
(imcad-preview-display-mod-commands adj :vpan
                                    (up . (* (cdr (imcad-preview-display-image-size)) (cdr imcad-preview-pan-step)))
                                    (down . (- (* (cdr (imcad-preview-display-image-size)) (cdr imcad-preview-pan-step)))))

(defvar imcad-preview-display-mode-map (make-sparse-keymap "IMCAD")
  "Keymap for `imcad-preview-display-mode'.")

(defmacro imcad-preview-display-define-keys (&rest pairs)
  `(progn
     ,@(mapcar (lambda (pair)
                 `(define-key imcad-preview-display-mode-map (kbd ,(car pair))
                    ',(intern (concat "imcad-preview-display-" (symbol-name (cdr pair))))))
               pairs)))

(imcad-preview-display-define-keys
 ;; mode
 ("s" . set-mode-shaded)
 ("w" . set-mode-wireframe)
 ("h" . set-mode-hlr)
 ;; proj
 ("C-o" . set-proj-ortho)
 ("C-p" . set-proj-persp)
 ;; view
 ("y" . set-view-iso)
 ("t" . set-view-top)
 ("T" . set-view-bottom)
 ("f" . set-view-front)
 ("F" . set-view-rear)
 ("l" . set-view-left)
 ("L" . set-view-right)
 ;; horizontal rotation (longitude)
 ("<left>" . adj-hrot-left)
 ("<right>" . adj-hrot-right)
 ;; vertical rotation (latitude)
 ("<up>" . adj-vrot-up)
 ("<down>" . adj-vrot-down)
 ;; horizontal pan
 ("C-<left>" . adj-hpan-left)
 ("C-<right>" . adj-hpan-right)
 ;; vertical pan
 ("C-<up>" . adj-vpan-up)
 ("C-<down>" . adj-vpan-down)
 ;; manage
 ("g" . reset-view)
 ("x" . restart))

(easy-menu-define imcad-preview-display-menu imcad-preview-display-mode-map
  "Menu for IMCAD preview window."
  '("IMCAD"
    ["Shaded mode" imcad-preview-display-set-mode-shaded
     :help "Set shaded mode"
     :style radio
     :selected (eq 'shaded (imcad-preview-display-param-get :mode))]
    ["Wireframe mode" imcad-preview-display-set-mode-wireframe
     :help "Set wireframe mode"
     :style radio
     :selected (eq 'wireframe (imcad-preview-display-param-get :mode))]
    ["HLR mode" imcad-preview-display-set-mode-hlr
     :help "Set HLR mode"
     :style radio
     :selected (eq 'hlr (imcad-preview-display-param-get :mode))]
    "--"
    ["Orthographic projection" imcad-preview-display-set-proj-ortho
     :help "Set orthographic projection"
     :style radio
     :selected (eq 'ortho (imcad-preview-display-param-get :proj))]
    ["Perspective projection" imcad-preview-display-set-proj-persp
     :help "Set perspective projection"
     :style radio
     :selected (eq 'persp (imcad-preview-display-param-get :proj))]
    "--"
    ["Isometric view" imcad-preview-display-set-view-iso
     :help "Set isometric view"]
    ["Top view" imcad-preview-display-set-view-top
     :help "Set top view"]
    ["Front view" imcad-preview-display-set-view-front
     :help "Set front view"]
    ["Left view" imcad-preview-display-set-view-left
     :help "Set left view"]
    ["Bottom view" imcad-preview-display-set-view-bottom
     :help "Set bottom view"]
    ["Rear view" imcad-preview-display-set-view-rear
     :help "Set rear view"]
    ["Right view" imcad-preview-display-set-view-right
     :help "Set right view"]
    "--"
    ["Rotate left" imcad-preview-display-adj-hrot-left]
    ["Rotate right" imcad-preview-display-adj-hrot-right]
    ["Rotate up" imcad-preview-display-adj-vrot-up]
    ["Rotate down" imcad-preview-display-adj-vrot-down]
    "--"
    ["Pan left" imcad-preview-display-adj-hpan-left]
    ["Pan right" imcad-preview-display-adj-hpan-right]
    ["Pan up" imcad-preview-display-adj-vpan-up]
    ["Pan down" imcad-preview-display-adj-vpan-down]
    "--"
    ["Reset view" imcad-preview-display-reset-view]
    ["Restart" imcad-preview-display-restart
     :help "Restart preview server"]
    ))

(define-derived-mode imcad-preview-display-mode nil
  "IMCAD Display"
  "IMCAD preview display mode."
  ;;(if imcad-preview-display-mode
  ;;    (imcad-preview-display-init)
  ;;  (imcad-preview-display-fini)))
  (imcad-preview-display-init))

(defun imcad-preview-display-init ()
  (setq buffer-read-only 't)
  (setq left-margin-width nil)
  (setq right-margin-width nil)
  (add-hook 'window-configuration-change-hook 'imcad-preview-display-refresh nil 't))

(defun imcad-preview-display-fini ()
  (remove-hook 'window-configuration-change-hook 'imcad-preview-display-refresh 't))

(defun imcad-preview-display-source-path ()
  "Get the source file path in display context"
  (buffer-file-name imcad-preview-editor-buffer))

(defun imcad-preview-display-image-path ()
  "Get the image file path in display context"
  (let ((source-path (imcad-preview-display-source-path)))
    (expand-file-name
     (concat (file-name-base source-path) ".png")
     (file-name-directory source-path))))

(defun imcad-preview-display-image-size ()
  "Get the image size in display context"
  (cons (window-pixel-width (get-buffer-window))
        (window-pixel-height (get-buffer-window))))

(defun imcad-preview-display-refresh ()
  (if (get-buffer-window) ;; do not refresh when preview buffer isn't visible
      (progn
        ;; try restart server when it stopped
        (imcad-preview-process-start-when-stopped)
        (imcad-preview-request (list (cons 'op "serve")
                                     (cons 'src (imcad-preview-display-source-path))
                                     (cons 'dst (imcad-preview-display-image-path))
                                     (cons 'size (let ((view-size (imcad-preview-display-image-size)))
                                                   (vector (car view-size) (cdr view-size))))
                                     (cons 'view "front")
                                     (cons 'proj (imcad-preview-display-param-get :proj))
                                     (cons 'mode (imcad-preview-display-param-get :mode))
                                     (cons 'zoom (if (imcad-preview-display-param-get :fit)
                                                     (list "fit" (imcad-preview-display-param-get :zoom))
                                                   (imcad-preview-display-param-get :zoom)))
                                     (cons 'rot (vector (imcad-preview-display-param-get :hrot)
                                                        (imcad-preview-display-param-get :vrot)))
                                     (cons 'pan (vector (imcad-preview-display-param-get :hpan)
                                                        (imcad-preview-display-param-get :vpan))))))))

(defun imcad-preview-refresh ()
  (with-current-buffer imcad-preview-viewer-buffer
    (imcad-preview-display-refresh)))

(defun imcad-preview-request (request)
  (if (imcad-preview-process-started)
      (progn ;; set viewer buffer for pending request
        ;; send request to the server process
        (let ((input (json-encode request)))
          (if imcad-preview-logging-io
              (message "IMCAD << %s" input))
          (process-send-string
           imcad-preview-process-handle
           (concat input "\n"))))
    (warn "Unable to request stopped process")))

(defun imcad-preview-request-forget (filename)
  (imcad-preview-request (list (cons 'op "forget")
                               (cons 'src filename))))

(defun imcad-preview-respond (response)
  ;; when we have pernding request
  (let* ((filename (alist-get 'src response))
         (editor-buffer (and filename
                             (get-file-buffer filename))))
    (if editor-buffer
        ;; process response
        (let ((viewer-buffer (buffer-local-value
                              'imcad-preview-viewer-buffer
                              editor-buffer)))
          (if (and viewer-buffer
                   (get-buffer viewer-buffer))
              (with-current-buffer viewer-buffer
                (imcad-preview-display-repaint response))
            ;; forget filename
            (imcad-preview-request-forget filename)))
      ;; forget filename
      (imcad-preview-request-forget filename))))

(defun imcad-preview-display-repaint (response)
  (setq buffer-read-only nil)
  (erase-buffer)
  (if (string= "good" (alist-get 'status response))
      (let ((image-spec (create-image
                         (imcad-preview-display-image-path))))
        (image-flush image-spec)
        (insert-image image-spec))
    (progn
      (insert (alist-get 'reason response))
      (let ((trace (alist-get 'trace response)))
        (if trace (progn (insert "\n\n")
                         (mapc (lambda (entry) (insert entry))
                               trace))))))
  (setq buffer-read-only 't))

(defun imcad-preview-process-started ()
  ;; check when handle is not nil and process is run
  (and imcad-preview-process-handle
       (process-live-p imcad-preview-process-handle)))

(defun imcad-preview-process-stopped ()
  (not (imcad-preview-process-started)))

(defun imcad-preview-process-start-when-stopped ()
  (if (imcad-preview-process-stopped)
      (progn
        (let* ((python-path (mapconcat 'identity
                                       (delq nil (cons (getenv "PYTHONPATH")
                                                       imcad-preview-python-path)) ":"))
               (process-environment (if (string-empty-p python-path) process-environment
                                      (cons (concat "PYTHONPATH=" python-path)
                                            process-environment)))
               (process-environment (if (string-empty-p imcad-preview-cache-dir) process-environment
                                      (cons (concat "IMCAD_CACHE_DIR=" imcad-preview-cache-dir)
                                            process-environment))))
          (if imcad-preview-logging-io
              (progn (message "IMCAD ENV: \n    %s" (mapconcat 'identity process-environment "\n    "))
                     (message "IMCAD CMD: %s" imcad-preview-command)))
          ;; start server process
          (setq imcad-preview-process-handle
                (make-process :name "IMCAD Server"
                              :buffer '()
                              :command (split-string imcad-preview-command " ")
                              :noquery 't
                              :filter 'imcad-preview-process-filter
                              :sentinel 'imcad-preview-process-sentinel)))
        ;; renew serving for active buffers
        ;; FIXME:
        ;; (mapc (lambda (editor-buffer)
        ;;         (with-current-buffer editor-buffer
        ;;           (imcad-preview-refresh)))
        ;;       (imcad-preview-mode-buffers (buffer-list)))
        't)))

(defun imcad-preview-process-stop-when-unused ()
  (if (not (imcad-preview-mode-buffers (buffer-list)))
      (imcad-preview-process-stop-when-started)))

(defun imcad-preview-process-stop-when-started ()
  (if (imcad-preview-process-started)
      (imcad-preview-request (list (cons 'op "stop")))
    (kill-process imcad-preview-process-handle))
  (setq imcad-preview-process-handle nil))

(defun imcad-preview-mode-buffers (buffers)
  "Get buffers where imcad-preview-mode is active."
  (if buffers
      (if (buffer-local-value 'imcad-preview-mode (car buffers))
          (cons (car buffers) (imcad-preview-mode-buffers (cdr buffers)))
        (imcad-preview-mode-buffers (cdr buffers)))
    nil))

(defun imcad-preview-process-filter (process output)
  (dolist (line (split-string output "\n"))
    (if (not (string-empty-p line))
        (imcad-preview-process-output line))))

(defun imcad-preview-process-output (output)
  (if imcad-preview-logging-io
      (message "IMCAD >> %s" output))
  (if (and (string-prefix-p "{" output)
           (string-suffix-p "}" output))
      (imcad-preview-respond
       (json-read-from-string output))))

(defun imcad-preview-process-sentinel (process event)
  ;; restart server when it crashed
  (if (and (string-match-p "finished" event)
           (not (= 0 (process-exit-status process))))
      (imcad-preview-process-start-when-stopped)))

(defun imcad-preview-display-restart ()
  "Restart preview server."
  (interactive "@")
  (imcad-preview-process-stop-when-started)
  (imcad-preview-display-refresh))

(defgroup imcad-preview nil
  "IMCAD preview"
  :group 'applications)

(defcustom imcad-preview-window-placement 'right
  "Preview window placement relative to editing window."
  :type '(radio (const :tag "Left" left)
                (const :tag "Right" right)
                (const :tag "Top" above)
                (const :tag "Bottom" below)
                (other :tag "Off" nil))
  :group 'imcad-preview)

(defcustom imcad-preview-window-proportion 0.5
  "Preview window size relative to parent window."
  :type '(float :min 0.1 :max 0.9)
  :group 'imcad-preview)

(defcustom imcad-preview-default-mode 'shaded
  "Default view mode."
  :type '(radio (const :tag "Shaded" shaded)
                (const :tag "Wireframe" wireframe)
                (const :tag "HLR" hlr))
  :group 'imcad-preview)

(defcustom imcad-preview-default-proj 'ortho
  "Default projection type."
  :type '(radio (const :tag "Orthographic" ortho)
                (const :tag "Perspective" persp))
  :group 'imcad-preview)

(defcustom imcad-preview-default-view 'iso
  "Default view plane."
  :type '(radio (const :tag "Isometric" iso)
                (const :tag "Top" top)
                (const :tag "Front" front)
                (const :tag "Left" left)
                (const :tag "Bottom" bottom)
                (const :tag "Rear" rear)
                (const :tag "Right" right))
  :group 'imcad-preview)

(defcustom imcad-preview-default-fit 't
  "Default fit all setting."
  :type '(radio (const :tag "Yes" t)
                (const :tag "No" nil))
  :group 'imcad-preview)

(defcustom imcad-preview-default-zoom 1.0
  "Default zoom factor."
  :type '(float)
  :group 'imcad-preview)

(defconst imcad-preview-rot-view-iso '(-45 . -45)
  "Isometric view")
(defconst imcad-preview-rot-view-front '(0 . 0)
  "Front view")
(defconst imcad-preview-rot-view-left '(90 . 0)
  "Left view")
(defconst imcad-preview-rot-view-top '(0 . -90)
  "Top view")
(defconst imcad-preview-rot-view-rear '(180 . 0)
  "Rear view")
(defconst imcad-preview-rot-view-right '(-90 . 0)
  "Right view")
(defconst imcad-preview-rot-view-bottom '(0 . 90)
  "Bottom view")

(defun imcad-preview-default-rot-get (name)
  (imcad-preview-default-rot-get-helper
   imcad-preview-default-rot
   '(imcad-preview-rot-view-iso
     imcad-preview-rot-view-front
     imcad-preview-rot-view-left
     imcad-preview-rot-view-top
     imcad-preview-rot-view-rear
     imcad-preview-rot-view-right
     imcad-preview-rot-view-bottom)))

(defun imcad-preview-default-rot-get-helper (value options)
  (let* ((option (symbol-value (car options))))
    (if (and (= (car value) (car option))
             (= (cdr value) (cdr option)))
        (car options)
      (if (cdr options)
          (imcad-preview-default-rot-get-helper value (cdr options))
        value))))

(defun imcad-preview-default-rot-set (name value)
  (setq imcad-preview-default-rot
        (if (symbolp value)
            (symbol-value value)
          value)))

(defcustom imcad-preview-default-rot 'imcad-preview-rot-view-iso
  "Initial latitude angle in degrees."
  :type '(radio (variable-item imcad-preview-rot-view-iso)
                (variable-item imcad-preview-rot-view-front)
                (variable-item imcad-preview-rot-view-left)
                (variable-item imcad-preview-rot-view-top)
                (variable-item imcad-preview-rot-view-rear)
                (variable-item imcad-preview-rot-view-right)
                (variable-item imcad-preview-rot-view-bottom)
                (cons :tag "Custom"
                      (float :tag "Longitude (Z)" :min -180.0 :max 180.0)
                      (float :tag "Latitude (X)" :min -90.0 :max 90.0)))
  :get 'imcad-preview-default-rot-get
  :set 'imcad-preview-default-rot-set
  :group 'imcad-preview)

(defcustom imcad-preview-rot-step '(10.0 . 5.0)
  "The angular steps for rotation of view in degrees."
  :type '(cons (float :tag "Longitude (Z)" :min 0.0 :max 180.0)
               (float :tag "Latitude (X)" :min 0.0 :max 90.0))
  :group 'imcad-preview)

(defcustom imcad-preview-default-pan '(0.0 . 0.0)
  "Initial relative pan offsets [X Y]."
  :type '(cons (float :tag "Horizontal (X)" :min -1.0 :max 1.0)
               (float :tag "Vertical (Y)" :min -1.0 :max 1.0))
  :group 'imcad-preview)

(defcustom imcad-preview-pan-step '(0.1 . 0.1)
  "The relative steps for horizontal and vertical view panning."
  :type '(cons (float :tag "Horizontal (X)" :min 0.0 :max 1.0)
               (float :tag "Vertical (Y)" :min 0.0 :max 1.0))
  :group 'imcad-preview)

(defcustom imcad-preview-command "python3 -m imcad"
  "A command to run IMCAD server."
  :type '(string)
  :group 'imcad-preview)

(defcustom imcad-preview-python-path '()
  "Extra python modules search paths."
  :type '(repeat directory)
  :group 'imcad-preview)

(defcustom imcad-preview-cache-dir ""
  "Persistent cache directory."
  :type '(directory)
  :group 'imcad-preview)

(defcustom imcad-preview-logging-io '()
  "Enable logging interaction with server for debugging purposes."
  :type '(boolean)
  :group 'imcad-preview)

(provide 'imcad-preview)
