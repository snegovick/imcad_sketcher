"""
Boolean operations on solids
"""

from OCC.Core.BRepAlgoAPI import BRepAlgoAPI_Fuse, BRepAlgoAPI_Common, BRepAlgoAPI_Cut

from .solid import SolidContext

class BooleanContext(SolidContext):
    """Boolean operation context"""

    def __init__(self, op=BRepAlgoAPI_Fuse):
        SolidContext.__init__(self)
        self.operation = op

    def process_solid(self):
        shape = self.elements[0].shape

        for element in self.elements[1:]:
            shape = self.operation(shape, element.shape).Shape()

        return shape

def union():
    """Create a union of solids"""
    return BooleanContext(BRepAlgoAPI_Fuse)

def difference():
    """Create a difference of solids"""
    return BooleanContext(BRepAlgoAPI_Cut)

def intersection():
    """Create an intersection of solids"""
    return BooleanContext(BRepAlgoAPI_Common)
