"""
Generic modeling contexts
"""

class BaseContext:
    """
    Base class for contexts
    """

    current = None
    """Current active context"""

    def __init__(self):
        self.parent = None
        self.elements = None

    def __enter__(self):
        self.elements = []
        self.parent = BaseContext.current

        print("enter " + self.__class__.__name__)

        self.open_context()

        BaseContext.current = self

    def __exit__(self, exception_type, exception_value, exception_traceback):
        BaseContext.current = self.parent

        print("leave " + self.__class__.__name__)

        self.close_context()

    @property
    def first_element(self):
        """Get first element"""
        if self.elements:
            return self.elements[0]
        return None

    @property
    def last_element(self):
        """Get last element"""
        if self.elements:
            return self.elements[len(self.elements)-1]
        return None

    def last_element_with_assert(self, element_type=None):
        """Get last element with assertion"""
        element = self.last_element
        if element is None:
            raise RuntimeError("Last element is missing")
        if element_type is not None:
            if not element.of_type(element_type):
                raise RuntimeError("Last element is not " + element_type.__name__)
        return element

    def put_element(self, element, replace=False):
        """Add new element"""
        if replace and self.elements:
            self.elements[len(self.elements)-1] = element
        else:
            self.elements.append(element)

    def put_elements(self, elements):
        """Add all elements"""
        self.elements.extend(elements)

    def transfer_elements_to_parent(self):
        """Transfer all elements from context to parent context when it possible"""
        if self.parent is not None:
            self.parent.put_elements(self.elements)

    def open_context(self):
        """Context openning hook"""
        #raise NotImplementedError()

    def close_context(self):
        """Context closing hook"""
        #raise NotImplementedError()
        self.transfer_elements_to_parent()

    def __iter__(self):
        return _ParentsIterator(self)

    def assert_some_elements(self, message):
        """Check some elements in context"""
        if not self.elements:
            raise RuntimeError(message)

    def assert_single_element(self, message):
        """Check single element in context"""
        if not self.elements:
            raise RuntimeError(message)

    def context_by_type(self, context_type):
        """Lookup parent context by type"""
        for context in self:
            if isinstance(context, context_type):
                return context
        return None

    def assert_in_context(self, context_type, message):
        """Check in active context"""
        if self.context_by_type(context_type) is None:
            raise RuntimeError(message)

    def assert_out_context(self, context_type, message):
        """Check out of active context"""
        if self.context_by_type(context_type) is not None:
            raise RuntimeError(message)

def root():
    """Create root context"""
    return BaseContext()

def current():
    """Get current active context"""
    return BaseContext.current

class _ParentsIterator:
    def __init__(self, context):
        self.context = context

    def __iter__(self):
        return self

    def __next__(self):
        context = self.context
        if context:
            self.context = self.context.parent
            return context
        raise StopIteration

class BaseElement:
    """
    Base class for elements
    """

    def transform(self, trsf):
        """Transform element"""
        raise NotImplementedError()

    def of_type(self, element_type):
        """Check when element of specified type"""
        return isinstance(self, element_type)

    def dumps(self):
        """Dumps element as raw data"""
        raise NotImplementedError()

    def loads(self, raw_data):
        """Loads element from raw data"""
        raise NotImplementedError()

    def clone(self):
        """Clone element"""
        raise NotImplementedError()

    def __getstate__(self):
        return self.dumps()

    def __setstate__(self, state):
        self.loads(state)
