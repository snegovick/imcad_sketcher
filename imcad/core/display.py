"""Display and GUI helpers
"""

from OCC.Display.OCCViewer import OffscreenRenderer
from OCC.Display.SimpleGui import init_display
from OCC.Display.WebGl.threejs_renderer import ThreejsRenderer
from OCC.Display.WebGl.x3dom_renderer import X3DomRenderer
from OCC.Core.V3d import V3d_X, V3d_Y, V3d_Z

from .context import BaseContext
from .utils import deg_to_rad

class DisplayContext(BaseContext):
    """Displaying context"""

    def __init__(self, backend=None, menu=False):
        print("new DisplayContext", backend)

        BaseContext.__init__(self)

        self.test = False

        self._proj = _Proj.default
        self._mode = _Mode.default
        self._size = (0, 0)

        self._view = _View.default
        self._zoom = _Zoom.default
        self._rot = (0, 0)
        self._pan = (0, 0)

        self._backend = backend
        self._menu = menu
        self._has_gui = backend not in (_Backend.X3DOM, _Backend.THREEJS)
        self._display = None
        self._mainloop = None

        self.on_init = dummy_fn
        self.on_view = dummy_fn
        self.on_done = dummy_fn

    @property
    def mode(self):
        """View mode"""
        return self._mode

    @mode.setter
    def mode(self, value):
        self._mode = value
        #if self._has_gui and self._display:
        #    _Mode.set(self._mode, self._display)

    @mode.deleter
    def mode(self):
        self.mode = _Mode.default

    @property
    def proj(self):
        """Propection type"""
        return self._proj

    @proj.setter
    def proj(self, value):
        self._proj = value
        #if self._has_gui and self._display:
        #    _Proj.set(self._proj, self._display)

    @proj.deleter
    def proj(self):
        self.proj = _Proj.default

    @property
    def size(self):
        """Viewport size"""
        return self._size

    @size.setter
    def size(self, value):
        self._size = (value[0], value[1])
        #if self._has_gui and self._display:
        #    self._display.SetSize(self._size[0], self._size[1])
        #    self._display.OverLayer.SetViewport(self._size[0], self._size[1])

    @property
    def width(self):
        """Viewport width"""
        return self.size[0]

    @width.setter
    def width(self, value):
        self.size = (value, self.size[1])

    @property
    def height(self):
        """Viewport height"""
        return self.size[1]

    @height.setter
    def height(self, value):
        self.size = (self.size[0], value)

    @property
    def view(self):
        """View plane"""
        return self._view

    @view.setter
    def view(self, value):
        self._view = value
        self._update_view()

    @view.deleter
    def view(self):
        self.view = _View.default

    @property
    def zoom(self):
        """View zoom"""
        return self._zoom

    @zoom.setter
    def zoom(self, value):
        self._zoom = value
        #self._update_view()

    @zoom.deleter
    def zoom(self):
        self.zoom = _Zoom.default

    @property
    def pan(self):
        """Panning offset"""
        return self._pan

    @pan.setter
    def pan(self, value):
        self._pan = (value[0], value[1])
        #self._update_view()

    @pan.deleter
    def pan(self):
        self.pan = (0, 0)

    @property
    def hor(self):
        """Hoizontal panning"""
        return self.pan[0]

    @hor.setter
    def hor(self, value):
        self.pan = (value, self.pan[1])

    @hor.deleter
    def hor(self):
        self.hor = 0

    @property
    def ver(self):
        """Vertical panning"""
        return self.pan[1]

    @ver.setter
    def ver(self, value):
        self.pan = (self.pan[0], value)

    @ver.deleter
    def ver(self):
        self.ver = 0

    @property
    def rot(self):
        """Rotation of view"""
        return self._rot

    @rot.setter
    def rot(self, value):
        self._rot = (value[0], value[1])
        #self._update_view()

    @rot.deleter
    def rot(self):
        self.rot = (0, 0)

    @property
    def lon(self):
        """Rotation longitude"""
        return self.rot[0]

    @lon.setter
    def lon(self, value):
        self.rot = (value, self.rot[1])

    @lon.deleter
    def del_lon(self):
        self.lon = 0

    @property
    def lat(self):
        """Rotation latitude"""
        return self.rot[1]

    @lat.setter
    def lat(self, value):
        self.rot = (self.rot[0], value)

    @lat.deleter
    def lat(self):
        self.lat = 0

    def _init_backend(self):
        if self._display:
            return

        display_, mainloop_, add_menu_, add_menu_function_ = \
            _Backend.init(self._backend)

        self._display = display_
        self._mainloop = mainloop_

        if add_menu_ is not None and add_menu_function_ is not None:
            _Menu.set(self._menu, add_menu_, add_menu_function_)

        if not self._size or self._size[0] == 0 or self._size[1] == 0:
            width, height = self._display.View.Window().Size()
            self._size = (width, height)

    def _update_view(self):
        if not self._has_gui or not self._display:
            return

        if self._size and self._size[0] > 0 and self._size[1] > 0:
            self._display.SetSize(self._size[0], self._size[1])
            self._display.OverLayer.SetViewport(self._size[0], self._size[1])

        self._display.ResetView()

        _Mode.set(self._mode, self._display)
        _Proj.set(self._proj, self._display)
        _View.set(self._view, self._display)

        #self._display.StartRotation(0, 0)
        #self._display.Rotation(int(self._rot[0]), int(self._rot[1]))

        #self._display.View.Rotate(V3d_X, deg_to_rad(self._rot[1]))
        #self._display.View.Rotate(V3d_Z, deg_to_rad(self._rot[0]))

        self._display.View.Rotate(deg_to_rad(self._rot[0]), deg_to_rad(self._rot[1]), 0.0)
        #self._display.View.Turn(deg_to_rad(self._rot[0]), deg_to_rad(self._rot[1]), 0.0)

        self._display.Pan(int(self._pan[0]), int(self._pan[1]))

        _Zoom.set(self._zoom, self._display)

    state = True
    """Status of display contexts"""

    @classmethod
    def enable(cls):
        """Enable display contexts"""
        cls.state = True

    @classmethod
    def disable(cls):
        """Disable display contexts"""
        cls.state = False

    def close_context(self):
        if not self.__class__.state:
            self.transfer_elements_to_parent()
            return

        if not self.test:
            self.assert_some_elements("No shapes to display")

        self._init_backend()

        self.on_init()

        self._display.EraseAll()

        if self.test:
            self._display.Test()
        else:
            for element in self.elements:
                self._display.DisplayShape(element.shape)

        self._update_view()

        self.on_view()

        self._mainloop()

        self.on_done()

        self.transfer_elements_to_parent()

    def snapshot(self, filename):
        """Take an image and save it to the file."""
        #self._display.ExportToImage(filename)
        self._display.View.Dump(filename)

    @property
    def image(self):
        """Get an image data."""
        self._display.GetImageData()

def dummy_fn(*_args):
    """Dummy function which does nothing"""

class _View:
    """View planes"""

    title = "view"
    options = {
        'iso': "Iso",
        'front': "Front",
        'rear': "Rear",
        'left': "Left",
        'right': "Right",
        'top': "Top",
        'bottom': "Bottom",
    }
    default = 'iso'

    @staticmethod
    def set(view, display_):
        """Setup view"""

        if view in ('iso', 'i'):
            display_.View_Iso()
        elif view in ('front', 'fnt', 'f'):
            display_.View_Front()
        elif view in ('rear', 'b'):
            display_.View_Rear()
        elif view in ('left', 'lft', 'l'):
            display_.View_Left()
        elif view in ('right', 'rht', 'r'):
            display_.View_Right()
        elif view in ('top', 't', 'u'):
            display_.View_Top()
        elif view in ('bottom', 'btm', 'd'):
            display_.View_Bottom()

class _Proj:
    """Projection modes"""

    title = "projection"
    options = {
        'ortho': "Orthographic",
        'persp': "Perspective",
    }
    default = 'ortho'

    @staticmethod
    def set(proj, display_):
        """Setup projection"""

        if proj in ('ortho', 'orthographic'):
            display_.SetOrthographicProjection()
        elif proj in ('persp', 'perspective'):
            display_.SetPerspectiveProjection()
        display_.Repaint()

class _Mode:
    """View modes"""

    title = "mode"
    options = {
        'shaded': "Shaded",
        'wireframe': "Wireframe",
        'hlr': "HLR",
    }
    default = 'shaded'

    @staticmethod
    def set(mode, display_):
        """Set view mode"""

        if mode in ('shaded', 'shd', 'sh', 's'):
            display_.SetModeShaded()
        elif mode in ('wireframe', 'wrf', 'wf', 'w'):
            display_.SetModeWireFrame()
        elif mode in ('hlr', 'h'):
            display_.SetModeHLR()

class _Zoom:
    """Zooming options"""

    title = "zoom"
    options = {
        'fit': "Fit All",
    }
    default = 'fit'

    @staticmethod
    def set(zoom, display_):
        """Set zoom"""
        if zoom is not None:
            if zoom == 'fit':
                display_.FitAll()
            elif isinstance(zoom, (tuple, list)):
                nums = len(zoom)
                if nums > 0 and zoom[0] == 'fit':
                    display_.FitAll()
                    zoom = zoom[1:]
                    nums -= 1
                if nums == 1:
                    display_.ZoomFactor(zoom[0])
                elif nums == 2:
                    display_.Zoom(zoom[0], zoom[1])
                elif nums == 4:
                    display_.ZoomArea(zoom[0], zoom[1], zoom[2], zoom[3])
                else:
                    raise RuntimeError("Invalid 'zoom' value")
            elif isinstance(zoom, (int, float)):
                display_.ZoomFactor(zoom)
            else:
                raise RuntimeError("Invalid 'zoom' value")

class _Menu:
    """Menu options"""

    VIEW = "View"
    PROJ = "Projection"
    MODE = "Mode"
    EXPORT = "Export"

    NONE = []
    ALL = [VIEW, PROJ, MODE, EXPORT]

    default = ALL

    @staticmethod
    def set(menu, add_menu_, add_menu_function_):
        """Set menu"""

        if menu is True:
            menu = _Menu.ALL

        if menu is None:
            menu = _Menu.NONE

        if not isinstance(menu, list):
            menu = [menu]

        if _Menu.VIEW in menu:
            _enum_menu(_View, add_menu_, add_menu_function_)

        if _Menu.PROJ in menu:
            _enum_menu(_Proj, add_menu_, add_menu_function_)

        if _Menu.MODE in menu:
            _enum_menu(_Mode, add_menu_, add_menu_function_)

        #if Menu.EXPORT.value in menu:
            #add_menu_("Export")
            #add_menu_function_("Save as STL", export_stl)
            #add_menu_function_("Save as STEP", export_step)
            #add_menu_function_("Save as PNG", save_png)
            #add_menu_function_("Save as JPEG", save_jpeg)

def _enum_menu(enum_, add_menu_, add_menu_function_):
    title = enum_.title
    add_menu_(title)
    for option in enum_.options:
        add_menu_function_(title, enum_.options[option], _menu_fn(enum_, option))

def _menu_fn(enum_, option):
    def func_(display_, _event):
        enum_.set(option, display_)
    return func_

class _Backend:
    """Display backend"""

    OFFSCREEN = "offscreen"
    WX = "wx"
    PYQT4 = "pyqt4"
    PYQT5 = "pyqt5"
    PYSIDE = "pyside"
    X3DOM = "x3dom"
    THREEJS = "threejs"

    @staticmethod
    def init(backend):
        """Initialize backend"""

        if backend in (_Backend.OFFSCREEN, _Backend.X3DOM, _Backend.THREEJS):
            display_ = None
            mainloop_ = None

            def do_render():
                display_.render()

            if backend == _Backend.OFFSCREEN:
                display_ = OffscreenRenderer()
                mainloop_ = dummy_fn
            elif backend == _Backend.X3DOM:
                display_ = X3DomRenderer()
                mainloop_ = do_render
            elif backend == _Backend.THREEJS:
                display_ = ThreejsRenderer()
                mainloop_ = do_render

            return display_, mainloop_, None, None

        display_, mainloop_, add_menu_, add_function_to_menu_ = \
            init_display(backend)

        def add_menu_function_(name, item, fn):
            def wrapped_fn(event_=None):
                fn(display_, event_)
            wrapped_fn.__name__ = item
            add_function_to_menu_(name, wrapped_fn)

        return display_, mainloop_, add_menu_, add_menu_function_

def display(view=None, proj=None, mode=None, size=None, \
            zoom=None, menu=True, test=False, backend=None):
    """Display shape

    Arguments:
    view -- the view plane (one of: 'iso', 'front', 'rear', 'left', 'right',
            'top', 'bottom', default: 'iso')
    proj -- the type of projection (one of: 'ortho', 'persp', default: 'ortho')
    mode -- the view mode (one of: 'shaded', 'wireframe', 'hlr', default: 'shaded')
    size -- the size of viewport (cortage (width, height), default: (1024, 768))
    zoom -- the zooming factor/dimensions/area (one of: 'fit', factor, (x, y),
            (x1, y1, x2, y2), default: 'fit')
    menu -- the menu display mode (one of: True, False, [...flag] where flag one of:
            'view', 'projection', 'mode'. default: True)
    test -- testing mode (default: False)
    backend -- the backend to use (one of: None, 'offscreen', 'wx', 'pyqt4', 'pyqt5',
               'pyside', 'x3dom', 'threejs'. default: None)
    """

    context = DisplayContext(backend, menu)

    if proj is not None:
        context.proj = proj
    if mode is not None:
        context.mode = mode
    if size is not None:
        context.size = size

    if test is not None:
        context.test = test

    if view is not None:
        context.view = view
    if zoom is not None:
        context.zoom = zoom

    return context

def render(filename, view='iso', proj='ortho', mode=None, \
           size=None, zoom='fit', test=False):
    """Render view to image

    Arguments:
    filename -- the name of file to save to
    view -- the view plane (one of: 'iso', 'front', 'rear', 'left', 'right',
            'top', 'bottom', default: 'iso')
    proj -- the type of projection (one of: 'ortho', 'persp', default: 'ortho')
    mode -- the view mode (one of: 'shaded', 'wireframe', 'hlr', default: 'shaded')
    size -- the size of viewport (cortage (width, height), default: (1024, 768))
    zoom -- the zooming factor/dimensions/area (one of: 'fit', factor, (x, y),
            (x1, y1, x2, y2), default: 'fit')
    test -- testing mode (default: False)
    """

    context = DisplayContext('offscreen')

    def on_view():
        context.snapshot(filename)

    context.on_view = on_view

    if proj is not None:
        context.proj = proj
    if mode is not None:
        context.mode = mode
    if size is not None:
        context.size = size

    if test is not None:
        context.test = test

    if view is not None:
        context.view = view
    if zoom is not None:
        context.zoom = zoom

    return context
