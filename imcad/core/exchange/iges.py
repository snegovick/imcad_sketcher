"""IGES data exchange
"""

from OCC.Core.IGESControl import IGESControl_Reader, IGESControl_Writer
from OCC.Core.BRep import BRep_Builder
from OCC.Core.TopoDS import TopoDS_Compound
from OCC.Core.IFSelect import IFSelect_RetDone

from .exchange import Reader, Writer

class IGES(Reader, Writer):
    """IGES topology format support"""

    name = "IGES"
    extensions = [".iges", ".igs"]

    @staticmethod
    def impl_read_file(filename):
        reader = IGESControl_Reader()

        status = reader.ReadFile(filename)

        if status is not IFSelect_RetDone:
            raise IOError("Error when reading IGES data")

        # transfer roots
        reader.TransferRoots()

        num_shapes = reader.NbShapes()

        shapes = []
        invalid_shapes = 0

        for i in range(1, num_shapes + 1):
            shape = reader.Shape(i)
            if shape.IsNull():
                invaid_shapes += 1
            else:
                shapes.append(shape)

        if invalid_shapes > 0:
            print("Unable to read %i shape(s)" % invalid_shapes)

        if not shapes:
            raise AssertionError("No shapes found")

        builder = BRep_Builder()

        compound = TopoDS_Compound()

        builder.MakeCompound(compound)

        for shape in shapes:
            builder.Add(compound, shape)

        return compound

    @staticmethod
    def impl_write_file(shape, filename, _options):
        writer = IGESControl_Writer()

        writer.AddShape(shape)

        status = writer.Write(filename)

        if not status:
            raise AssertionError("Error when writing IGES data")

IGES.register_reader()
IGES.register_writer()
