"""STEP data exchange
"""

from OCC.Core.STEPControl import STEPControl_Reader, STEPControl_Writer, STEPControl_AsIs
from OCC.Core.Interface import Interface_Static_SetCVal
from OCC.Core.IFSelect import IFSelect_RetDone

from .exchange import Reader, Writer

class STEPOptions:
    """STEP processor options"""

    def __init__(self, schema='AP214IS'):
        """Initialize STEP processor options.

        Arguments:
        schema -- the application protocol to use for writing files (either 'AP203', 'AP214IS')
        """
        self.schema = schema

class STEP(Reader, Writer):
    """STEP topology format support"""

    name = "STEP"
    extensions = [".step", ".stp"]

    @staticmethod
    def impl_read_file(filename):
        reader = STEPControl_Reader()

        status = reader.ReadFile(filename)

        if status is not IFSelect_RetDone:
            raise IOError("Error when reading STEP")

        result = reader.TransferRoot()

        if not result:
            raise AssertionError("Unable to transfer root")

        num_shapes = reader.NbShapes()

        if num_shapes < 1:
            raise AssertionError("No shapes found")

        if num_shapes > 1:
            raise AssertionError("Multiple shapes in root")

        # get shape
        shape = reader.Shape(1)

        return shape

    @staticmethod
    def impl_write_file(shape, filename, options):
        writer = STEPControl_Writer()

        Interface_Static_SetCVal("write.step.schema", options.schema)

        # transfer shapes as is
        writer.Transfer(shape, STEPControl_AsIs)

        # write file
        status = writer.Write(filename)

        if status is not IFSelect_RetDone:
            raise AssertionError("Error while writing STEP file")

STEP.register_reader()
STEP.register_writer()
