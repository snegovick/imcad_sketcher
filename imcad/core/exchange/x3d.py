"""X3D data exchange
"""

from OCC.Core.BRepMesh import BRepMesh_IncrementalMesh
from OCC.Core.Visualization import Tesselator

from .exchange import Writer, MeshOptions

class X3DOptions(MeshOptions):
    """X3D processor options"""

class X3D(Writer):
    """X3D format support"""

    name = "X3D"
    extensions = [".x3d", ".wrl"]

    @staticmethod
    def impl_write_file(shape, filename, options):
        # meching the shape

        mesh = BRepMesh_IncrementalMesh(shape, options.linear_deflection, \
                                        options.deflection_relative, \
                                        options.angular_deflection, True)

        mesh.Perform()

        if not mesh.IsDone():
            raise AssertionError("Unable to generate mesh for shape")

        tesselator = Tesselator(shape)

        tesselator.Compute()

        tesselator.ExportShapeToX3D(filename)

X3D.register_writer()
