"""
Commonly used OCC helpers
"""

from OCC.Core.BRep import BRep_Builder
from OCC.Core.BRepTools import BRepTools_ShapeSet

def dumps(shape):
    """Dumps shape geometry as raw data"""

    shape_set = BRepTools_ShapeSet()
    shape_set.Add(shape)

    #return "DBRep_DrawableShape\n" +
    return shape_set.WriteToString()

def loads(raw_data):
    """Loads shape geometry from raw data"""

    shape_set = BRepTools_ShapeSet(BRep_Builder())
    shape_set.ReadFromString(raw_data)

    return shape_set.Shape(shape_set.NbShapes())
