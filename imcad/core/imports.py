"""Importing topology from files
"""

from .context import current
from .cache import put_validation, FileMTime
from .exchange import Reader
from .solid import SolidElement

def imports(filename):
    """Import geometry from CAD file (.step, .iges, .stl are supported)"""

    context = current()

    shape = Reader.read_file(filename)

    context.put_element(SolidElement(shape))

    put_validation(FileMTime(filename))
