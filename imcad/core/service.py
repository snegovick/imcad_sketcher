"""Service for continuous editing
"""

from json import loads, dumps, JSONDecodeError
from os.path import splitext, basename, dirname
from time import perf_counter, sleep
from sys import exc_info, modules, version_info
from traceback import format_exception
from multiprocessing.dummy import Pool
from watchdog.observers import Observer

from .display import DisplayContext
from .export import ExportContext
#from .transform import rotate
#from .utils import deg_to_rad

if version_info[0] < 3:
    from imp import load_source as _load_source
elif version_info[0] == 3 and version_info[1] <= 5:
    from importlib.machinery import SourceFileLoader

    def _load_source(module_name, file_path):
        return SourceFileLoader(module_name, file_path).load_module(module_name)
else:
    from importlib.util import spec_from_file_location, module_from_spec

    def _load_source(module_name, file_path):
        spec = spec_from_file_location(module_name, file_path)
        module = module_from_spec(spec)
        spec.loader.exec_module(module)
        return module

if version_info[0] == 3 and version_info[1] <= 4:
    from imp import reload
else:
    from importlib import reload

class Service:
    """Basic service implementation"""

    def __init__(self, input_stream, output_stream, min_frame_interval=0.25):
        self.input_stream = input_stream
        self.output_stream = output_stream
        self.min_frame_interval = min_frame_interval
        self.serving_pool = {}
        self.workers_pool = Pool(processes=1)
        self.running = False

    def run(self):
        """Process input stream run commands and output results"""

        self.running = True

        for line in self.input_stream:
            try:
                data = loads(line)
            except JSONDecodeError as e:
                self.error("Invalid JSON", {'message': e.msg, 'position': e.pos})
                continue

            self.process(data)

            if not self.running:
                break

    def process(self, data):
        """Process parsed command"""

        if 'op' not in data:
            self.error("Missing 'op' field")
            return

        op = data['op']

        if op == 'stop':
            self.running = False
            return

        if op not in ('serve', 'forget'):
            self.error("Unknown operation '%s'" % op)

        if 'src' not in data:
            self.error("Missing 'src' field which should contain path to the source code")
            return

        src = data['src']

        if op == 'serve':
            if 'dst' not in data:
                self.error("Missing 'dst' field which should contain path to the destination image")

            dst = data['dst']

            self.handle_serve(src, dst, data)
        elif op == 'forget':
            self.handle_forget(src)

    def handle_serve(self, src, dst, data):
        """Handle 'serve' request"""

        if src in self.serving_pool:
            entry = self.serving_pool[src]
        else:
            entry = Entry(self, src, dst)
            self.serving_pool[src] = entry

        entry.view(data)
        entry.async_render()

    def handle_forget(self, src):
        """Handle 'unserve' request"""

        if src not in self.serving_pool:
            self.error("Source '%s' isn't served", src)
            return

        del self.serving_pool[src]

    def success(self, data=None):
        """Outputs success status"""

        if data is None:
            data = {}

        data['status'] = 'good'

        self.output(data)

    def error(self, reason, data=None):
        """Outputs error"""

        if data is None:
            data = {}

        data['status'] = 'bad'
        data['reason'] = reason

        self.output(data)

    def output(self, data):
        """Outputs data"""

        self.output_stream.write(dumps(data))
        self.output_stream.write('\n')
        self.output_stream.flush()

class Entry:
    """Serving entry"""

    def __init__(self, service, source_path, image_path):
        self.service = service
        self.source_path = source_path
        self.image_path = image_path
        self.source_files = [self.source_path]
        self.reload_files = []
        self.context = DisplayContext('offscreen')
        self.watches = {}
        self.observer = Observer()
        self.rendering = False
        self.need_render = False
        self.last_render_time = None

    def _update_watches(self):
        paths_for_watch = []

        # collect unique path for watch
        for filename in self.source_files:
            path = dirname(filename)
            if path not in paths_for_watch:
                paths_for_watch.append(path)

        started = bool(self.watches)

        for path in self.watches:
            if path in paths_for_watch:
                paths_for_watch.remove(path)
            else:
                self.observer.unschedule(self.watches[path])
                del self.watches[path]
                print("Unwatch path %s" % path)

        if not paths_for_watch:
            if not self.watches and started:
                print("Stop watcher")
                # stop observer
                self.observer.stop()
            return

        for path in paths_for_watch:
            # schedule watcher
            self.watches[path] = self.observer.schedule(self, path, recursive=False)
            print("Watch path %s" % path)

        if not started:
            print("Start watcher")
            # start observer
            self.observer.start()

    def view(self, data):
        """Set view parameters from dict"""

        if 'mode' in data:
            self.context.mode = data['mode']
        if 'proj' in data:
            self.context.proj = data['proj']
        if 'size' in data:
            self.context.size = data['size']
        if 'view' in data:
            self.context.view = data['view']
        if 'rot' in data:
            self.context.rot = data['rot']
        if 'pan' in data:
            self.context.pan = data['pan']
        if 'zoom' in data:
            self.context.zoom = data['zoom']

    def render(self):
        """Trigger rendering"""

        context = self.context

        _reload_modules_with_files(self.reload_files)
        self.reload_files = []

        try:
            with context: #, rotate(a=pitch, xyz=(1, 0, 0)), rotate(a=yaw):
                DisplayContext.disable()
                ExportContext.disable()

                module, loaded_modules = _exec_source(self.source_path)

                if 'preview' in module.__dict__:
                    preview()

                DisplayContext.enable()
                ExportContext.enable()
        except:
            ex_type, ex, tb = exc_info()
            self.error("Source error", {'trace': format_exception(ex_type, ex, tb)})

        # extra delay to get time for reload image
        if self.last_render_time is not None:
            frame_interval = perf_counter() - self.last_render_time
            extra_delay = self.service.min_frame_interval - frame_interval
            if extra_delay > 0.0:
                sleep(extra_delay)

        context.snapshot(self.image_path)

        self.source_files.extend(_modules_files(loaded_modules))

        self._update_watches()

        self.success({
            'mode': context.mode,
            'size': context.size,
            'proj': context.proj,
            'view': context.view,
            'zoom': context.zoom,
            'rot': context.rot,
            'pan': context.pan,
        })

        self.last_render_time = perf_counter()
        self.rendering = False

        if self.need_render:
            self.need_render = False
            self.async_render()

    def async_render(self):
        """Trigger rendering asynchronously"""
        if not self.rendering:
            self.rendering = True
            self.service.workers_pool.apply_async(self.render, [])
        else:
            self.need_render = True

    def dispatch(self, event):
        """Dispatch filesystem events"""

        if event.src_path in self.source_files:
            self.reload_files.append(event.src_path)
            self.async_render()

    def success(self, data=None):
        """Outputs success status"""

        if data is None:
            data = {}

        data['src'] = self.source_path
        data['dst'] = self.image_path

        self.service.success(data)

    def error(self, reason, data=None):
        """Outputs error"""

        if data is None:
            data = {}

        data['src'] = self.source_path
        data['dst'] = self.image_path

        self.service.error(reason, data)

def _exec_source(path):
    name, _ = splitext(basename(path))

    # loaded modules snapshot
    origin_modules = list(modules.keys())

    module = _load_source(name, path)

    loaded_modules = []

    for module_name in modules:
        if module_name not in origin_modules:
            loaded_modules.append(module_name)

    return module, loaded_modules

def _unload_modules(modules_list):
    for module_name in modules:
        if module_name in modules_list:
            del modules[module_name]

def _reload_modules(modules_list):
    for module_name in modules:
        if module_name in modules_list:
            reload(modules[module_name])

def _reload_modules_with_files(files_list):
    for module_name in modules:
        if '__file__' in modules[module_name].__dict__ \
           and modules[module_name].__file__ in files_list:
            reload(modules[module_name])

def _modules_files(modules_list):
    loaded_files = []

    for module_name in modules:
        if module_name in modules_list \
           and '__file__' in modules[module_name].__dict__:
            loaded_files.append(modules[module_name].__file__)

    return loaded_files
