"""
Sketching tools
"""

from math import pi as PI

from OCC.Core.gp import gp, gp_Pnt, gp_Pln, gp_Circ
from OCC.Core.BRepBuilderAPI import BRepBuilderAPI_MakeEdge, \
    BRepBuilderAPI_MakeWire, BRepBuilderAPI_MakeFace
from OCC.Core.ChFi2d import ChFi2d_FilletAPI, ChFi2d_ChamferAPI
#from OCC.Core.GC import GC_MakeArcOfCircle

from .context import BaseContext, BaseElement, current
from .utils import same_point

def assert_out_sketch():
    """Check out of sketch"""
    current().assert_out_context(SketchContext, "Already in sketch")

def assert_in_sketch():
    """Check in sketching context"""
    current().assert_in_context(SketchContext, "Out of sketch")

def assert_in_profile():
    """Check in profile"""
    current().assert_in_context(ProfileContext, "Out of profile")

def assert_in_sketch_out_profile():
    """Check in sketch but out of profile"""
    assert_in_sketch()
    current().assert_out_context(ProfileContext, "Already in profile")

class SketchContext(BaseContext):
    """Base class for sketching contexts"""

    def __init__(self):
        BaseContext.__init__(self)

    def open_context(self):
        assert_out_sketch()

    #def close_context(self):
    #    current().put_element(SketchElement(self.process_sketch()))

    def process_sketch(self):
        """Create face from edges"""

        self.assert_some_elements("Empty sketch")

        face = BRepBuilderAPI_MakeFace(gp_Pln(), self.elements[0].shape)

        for element in self.elements[1:]:
            face.Add(element.shape)

        return face.Face()

def sketch():
    """Create sketch context"""
    return SketchContext()

class ProfileElement(BaseElement):
    """Base class for profile elements"""

    def __init__(self, shape):
        BaseElement.__init__(self)
        self.shape = shape

    def transform(self, trsf):
        return self.__class__(self.shape.Transformed(trsf))

    def clone(self):
        return self.__class__(self.shape)

class ProfileContext(BaseContext):
    """Profile context"""

    def open_context(self):
        assert_in_sketch_out_profile()

    def close_context(self):
        # Fix path by removing same end points
        # or adding extra line segment between
        # different end points.
        elements = self.elements
        index0 = 0

        while index0 < len(elements):
            index1 = index0 + 1
            if index1 >= len(elements):
                index1 = 0
            element0 = elements[index0]
            element1 = elements[index1]
            if element0.of_type(BasePoint) and element1.of_type(BasePoint):
                if element0.same_as(element1):
                    elements.pop(index0)
                    continue
                else:
                    elements.insert(index1, LineSegment())
                    index0 += 1
            index0 += 1

        # process geometry
        processable = True

        while processable:
            processable = False
            for index, element in enumerate(elements):
                if element.processable:
                    processable = True
                    element.process(elements, index)

        wire_ = BRepBuilderAPI_MakeWire()

        # collect edges
        for element in elements:
            if element.edges is not None and element.edges:
                for edge in element.edges:
                    wire_.Add(edge)

        current().put_element(ProfileElement(wire_.Wire()))

class SubProfileContext(BaseContext):
    """Sub-profile context"""

    def __init__(self, reverse_=False):
        BaseContext.__init__(self)
        self.reverse = reverse_

    def open_context(self):
        assert_in_profile()

    def close_context(self):
        if self.reverse:
            self.elements.reverse()

        current().put_elements(self.elements)

def reverse(r=True):
    """Creates sub profile context with reversing elements order"""
    return SubProfileContext(bool(r))

class _CutContext(BaseContext):
    def __init__(self, reverse_=False):
        BaseContext.__init__(self)
        self.reverse = reverse_

    def open_context(self):
        assert_in_sketch_out_profile()

    def close_context(self):
        if self.reverse:
            for element in self.elements:
                element.shape.Reverse()
        current().put_elements(self.elements)

def cut(cut_=True):
    """Create cut context"""
    return _CutContext(cut_)

def circle(r=None, d=None, x=0, y=0, xy=None):
    """Create circle"""
    assert_in_sketch_out_profile()

    if r is None and d is None:
        raise RuntimeError("Either circle radius(r) or diameter(d) should be specified")

    radius = r if r is not None else d * 0.5
    circ = gp_Circ(gp.XOY(), radius)
    circ.SetLocation(point_from_coords(x, y, xy))

    edge = BRepBuilderAPI_MakeEdge(circ, 0, PI*2)
    wire = BRepBuilderAPI_MakeWire(edge.Edge())

    current().put_element(ProfileElement(wire.Wire()))

def profile():
    """Create profile context"""
    return ProfileContext()

def point_from_coords(x=None, y=None, xy=None):
    """Create point using two coordinate values or vector"""
    if xy is not None:
        x = xy[0]
        y = xy[1]
    if x is None or y is None:
        raise RuntimeError("Both X and Y values should be specified")
    return gp_Pnt(x, y, 0)

class BasePoint(BaseElement):
    """Base point element class"""

    def __init__(self, p):
        self.pnt = p
        self.processable = False
        self.edges = None

    def same_as(self, point):
        """Check when point is same as other point"""
        return same_point(self.pnt, point.pnt)

    def transform(self, trsf):
        return self.__class__(self.pnt.Transformed(trsf))

    def clone(self):
        return self.__class__(self.pnt)

def move(x=0, y=0, xy=None):
    """Set starting point of wire"""
    assert_in_profile()
    current().put_element(BasePoint(point_from_coords(x, y, xy)))

def _get_element(elements, index):
    while index < 0:
        index += len(elements)
    while index >= len(elements):
        index -= len(elements)
    return elements[index]

class LineSegment(BaseElement):
    """Stright line segment"""

    def __init__(self):
        self.processable = True
        self.edges = None

    def process(self, elements, index):
        """Process element"""
        self.processable = False

        prev_point = _get_element(elements, index - 1).pnt
        next_point = _get_element(elements, index + 1).pnt

        self.edges = [BRepBuilderAPI_MakeEdge(prev_point, next_point).Edge()]

    def transform(self, trsf):
        return self

def line(x=None, y=None, xy=None):
    """Draw a line to specified point"""
    assert_in_profile()

    current().put_element(LineSegment())
    current().put_element(BasePoint(point_from_coords(x, y, xy)))

#class ArcSegment(BaseElement):
#    def __init__(self, r, a, a1, a2, s, p, mp):
#        self.arc = arc

#def arc(r=None, a=None, x=None, y=None, xy=None):
#    """Draw an arc with specified radius and angle"""
#
#    p = make_point(x, y, xy)
#
#    current.put(LineSegment())

class FilletPoint(BasePoint):
    """Filleted point class"""

    def __init__(self, p, r):
        BasePoint.__init__(self, p)
        self.radius = r
        self.processable = True
        self.processing = False

    def process(self, elements, index):
        """Process fillet"""
        if self.processing:
            self.processable = False

            prev_element = _get_element(elements, index - 1)
            next_element = _get_element(elements, index + 1)

            first_edge = prev_element.edges[len(prev_element.edges)-1]
            second_edge = next_element.edges[0]

            fillet_ = ChFi2d_FilletAPI(first_edge, second_edge, gp_Pln())
            fillet_.Perform(self.radius)
            point = gp_Pnt()
            edge = fillet_.Result(point, first_edge, second_edge)

            self.edges = [edge]
        else:
            self.processing = True

    def transform(self, trsf):
        return self.__class__(self.pnt.Transformed(trsf), self.radius)

def fillet(r=None, d=None):
    """Create fillet of specified radius at current point"""

    assert_in_profile()

    if r is None and d is None:
        raise RuntimeError("Either radius(r) or diameter(d) should be specified")
    r = r if r is not None else d * 0.5

    if r > 0.0:
        last_point = current().last_element_with_assert(BasePoint)
        current().put_element(FilletPoint(last_point.pnt, r), True)

class ChamferPoint(BasePoint):
    """Chamfered point class"""

    def __init__(self, p, l1, l2):
        BasePoint.__init__(self, p)
        self.length1 = l1
        self.length2 = l2
        self.processable = True
        self.processing = False

    def process(self, elements, index):
        """"Process chamfer"""
        if self.processing:
            self.processable = False

            prev_element = _get_element(elements, index - 1)
            next_element = _get_element(elements, index + 1)

            first_edge = prev_element.edges[len(prev_element.edges)-1]
            second_edge = next_element.edges[0]

            chamfer_ = ChFi2d_ChamferAPI(first_edge, second_edge)
            chamfer_.Perform()
            edge = chamfer_.Result(first_edge, second_edge, self.length1, self.length2)

            self.edges = [edge]
        else:
            self.processing = True

    def transform(self, trsf):
        return self.__class__(self.pnt.Transformed(trsf), self.length1, self.length2)

def chamfer(l=None, l1=None, l2=None):
    """Create chemfer of specified length(es) at current point"""

    assert_in_profile()

    l = l if l is not None else 0.0
    l1 = l1 if l1 is not None else l
    l2 = l2 if l2 is not None else l

    if l1 > 0.0 and l2 > 0.0:
        last_point = current().last_element_with_assert(BasePoint)
        current().put_element(ChamferPoint(last_point.pnt, l1, l2), True)
