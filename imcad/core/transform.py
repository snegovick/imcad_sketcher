"""
Affine 2D and 3D transformations
"""

from OCC.Core.gp import gp_Pnt, gp_Dir, gp_Ax1, gp_Vec, gp_Trsf
from .context import BaseContext, current
from .utils import deg_to_rad

class TransformContext(BaseContext):
    """Transformation context class"""

    def __init__(self, trsf):
        BaseContext.__init__(self)

        # TODO: Optimize nested transformations
        self.trsf = trsf

    def close_context(self):
        # TODO: Optimize nested transformations
        # Apply transformation and put elements to current context
        context = current()
        for element in self.elements:
            context.put_element(element.transform(self.trsf))

def decart_coords(x, y, z, xy, yz, xz, xyz, dx=0, dy=0, dz=0):
    """Get X, Y and Z from all supported variants with defaults"""

    if xyz is not None:
        x = xyz[0]
        y = xyz[1]
        z = xyz[2]
    else:
        if xy is not None:
            x = xy[0]
            y = xy[1]
        if yz is not None:
            y = yz[0]
            z = yz[1]
        if xz is not None:
            x = xz[0]
            z = xz[1]
    if x is None:
        x = dx
    if y is None:
        y = dy
    if z is None:
        z = dz
    return x, y, z

def translate(x=None, y=None, z=None, xy=None, yz=None, xz=None, xyz=None):
    """Translate coordinate basis"""
    x, y, z = decart_coords(x, y, z, xy, yz, xz, xyz)

    trsf = gp_Trsf()
    trsf.SetTranslation(gp_Vec(x, y, z))

    return TransformContext(trsf)

def rotate(a=0.0, x=None, y=None, z=None, xy=None, yz=None, xz=None, xyz=None):
    """Rotate coordinate basis"""
    x, y, z = decart_coords(x, y, z, xy, yz, xz, xyz, 0, 0, 1)
    trsf = gp_Trsf()

    try:
        d = gp_Dir(x, y, z)
        p = gp_Pnt()
        trsf.SetRotation(gp_Ax1(p, d), deg_to_rad(a))
    except:
        pass

    return TransformContext(trsf)

def viewplane(p="iso"):
    """Rotate for the specified view plane.

    Arguments:
    p -- View plane name ('front', 'left', 'top', 'rear', 'right', 'bottom' and combinations)"""
    return rotate()

def mirror(x=None, y=None, z=None, xy=None, yz=None, xz=None, xyz=None):
    """Mirror coordinate basis"""
    x, y, z = decart_coords(x, y, z, xy, yz, xz, xyz)
    trsf = gp_Trsf()

    try:
        d = gp_Dir(x, y, z)
        p = gp_Pnt()
        trsf.SetMirror(gp_Ax1(p, d))
    except:
        pass

    return TransformContext(trsf)
