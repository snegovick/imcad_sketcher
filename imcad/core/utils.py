"""
Math and geometry utilities
"""

from math import sqrt, pi as PI

RESOLUTION = 1e-13

_DEG_TO_RAD = PI / 180.0
_RAD_TO_DEG = 180.0 / PI

def deg_to_rad(a):
    return a * _DEG_TO_RAD

def rad_to_deg(a):
    return a * _RAD_TO_DEG

_TO_SQR_DIAG = 2.0 / sqrt(2.0)
_FROM_SQR_DIAG = sqrt(2.0) / 2.0

def to_sqr_diag(x):
    return x * _TO_SQR_DIAG

def from_sqr_diag(x):
    return x * _FROM_SQR_DIAG

def same_point(a, b):
    return a.IsEqual(b, RESOLUTION)
