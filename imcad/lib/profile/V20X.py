from imcad import profile, line, move, circle, fillet, reverse, cut, mirror, rotate, to_sqr_diag

class V20X:
    """V20X Profile model definition"""
    def __init__(self,
                 outer_width=20,
                 inner_width=10.6,
                 inner_depth=6,
                 corner_radius=1.5,
                 rail_outer=9.5,
                 rail_inner=6.2,
                 rail_thickness=2,
                 core_diameter=5,
                 support_thickness=1.8):
        """Create V20X profile model"""
        self.outer_width = outer_width
        self.inner_width = inner_width
        self.inner_depth = inner_depth
        self.corner_radius = corner_radius
        self.rail_outer = rail_outer
        self.rail_inner = rail_inner
        self.rail_thickness = rail_thickness
        self.core_diameter = core_diameter
        self.support_thickness = support_thickness
        self.little_radius = 0.1
        self.big_radius = 0.9

    @property
    def half_outer_width(self):
        return self.outer_width * 0.5

    @property
    def half_inner_width(self):
        return self.inner_width * 0.5

    @property
    def half_support_thickness(self):
        return self.support_thickness * 0.5

    @property
    def half_rail_outer(self):
        return self.rail_outer * 0.5

    @property
    def half_rail_inner(self):
        return self.rail_inner * 0.5

    @property
    def depth_offset(self):
        return self.half_outer_width - self.inner_depth

    def profile(self):
        with profile():
            for rotate_axis in [0, -90, -180, -270]:
                for mirror_diagon in [0, 1]:
                    with rotate(a=rotate_axis), \
                         mirror(mirror_diagon, mirror_diagon), \
                         reverse(mirror_diagon):
                        self.half_outer_segment()
        with cut():
            circle(d=self.core_diameter)

    def half_outer_segment(self):
        move(0,
             self.depth_offset)
        line(self.depth_offset - to_sqr_diag(self.half_support_thickness),
             self.depth_offset)
        fillet(self.big_radius)
        line(self.half_inner_width,
             self.half_inner_width + to_sqr_diag(self.half_support_thickness))
        fillet(self.big_radius)
        line(self.half_inner_width,
             self.half_outer_width - self.rail_thickness)
        fillet(self.little_radius)
        line(self.half_rail_inner,
             self.half_outer_width - self.rail_thickness)
        fillet(self.little_radius)
        line(self.half_rail_outer,
             self.half_outer_width)
        fillet(self.little_radius)
        line(self.half_outer_width,
             self.half_outer_width)
        fillet(self.corner_radius)

    def half_inner_segment(self):
        pass

V20X20 = V20X()
#V20X40 = V20XModel(x_dimensions=2)
#V20X60 = V20XModel(x_dimensions=3)
#V20X80 = V20XModel(x_dimensions=4)
