from imcad import extrude, cached
from .V20X import *

class ExtrudedProfile:
    """Generic extruded profile with given model and length"""
    def __init__(self, model, length=0.0):
        self.model = model
        self.length = length

    @cached
    def body(self):
        """Generate body"""
        with extrude(self.length):
            self.model.profile()
